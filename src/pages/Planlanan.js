import React from "react";
import Header from "../Components/Header";
import Footer from "../Components/Footer";
import Card from "react-bootstrap/Card";
import CardGroup from "react-bootstrap/CardGroup";

const Planlanan = () => {
  return (
    <>
      <Header></Header>
      <div className='header-title'>
        <span>PLANLANAN PROJELER</span>
      </div>
      <div className='projects-div'>
        <CardGroup className='card-group'>
          <Card className='projects-card'>
            <Card.Img
              variant='top'
              src='images/a.jpeg'
              className='card-image'
            />
            <Card.Body>
              <Card.Title>Proje 1</Card.Title>
              <Card.Link href='/' className='card-link'>
                Proje Detayı
              </Card.Link>
            </Card.Body>
          </Card>
          <Card className='projects-card'>
            <Card.Img
              variant='top'
              src='images/a.jpeg'
              className='card-image'
            />
            <Card.Body>
              <Card.Title>Proje 2</Card.Title>
              <Card.Link href='/' className='card-link'>
                Proje Detayı
              </Card.Link>
            </Card.Body>
          </Card>
          <Card className='projects-card'>
            <Card.Img
              variant='top'
              src='images/a.jpeg'
              className='card-image'
            />
            <Card.Body>
              <Card.Title>Proje 3</Card.Title>
              <Card.Link href='/' className='card-link'>
                Proje Detayı
              </Card.Link>
            </Card.Body>
          </Card>
        </CardGroup>
      </div>
      <Footer></Footer>
    </>
  );
};

export default Planlanan;
